## Context
<!-- Add any relevant context: issue background, who has requested the work, any research findings, data, a problem statement, business requirements etc -->

## User story
<!-- Who is trying to do what and why -->
**As a**  
**I want**  
**So that**  

## Solution
<!-- If we are proposing a solution, include details of how it will address the problem we are trying to solve -->

## Acceptance criteria
<!-- Include any relevant acceptance criteria -->

1.   
2.   
3.  


## Links and references
<!-- Provide links to any relevant files, artefacts or data -->
- [(Name of artefact)](url)


## Ready for refinement
<!-- The issue is ready for refinement once these items have been checked -->
- [ ] There is relevant context to the issue
- [ ] There is a user story and/or a problem statement
- [ ] The requirements are clear
- [ ] There are appropriate acceptance criteria
- [ ] Relevant links and references are available _(optional)_

/label ~"priority::pending"
/label ~"workflow::Needs Refinement" 
/label ~"team::Research and Design"
