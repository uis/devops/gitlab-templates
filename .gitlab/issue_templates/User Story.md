## Theme
<!--Grouping of user stories that share one or more common attribute(s)-->

## User story
<!--As a (user group) I need to (perform a specific action) so that (I can achieve a desired outcome).-->

## User goal
<!--Descriptions of end states that users want to reach -->

## User tasks
<!--Activities that the user must perform to accomplish the goal.-->

## Role
<!--User group(s)/profile-->

## Acceptance criteria
<!--Set of predefined requirements that must be met in order to mark a user story complete-->

## Solution
<!--Solving a problem or dealing with the situation.-->


/label ~"priority::pending"
/label ~"workflow::Needs Refinement"
/label ~"team::DevOps"
/label ~"issuetype::User Story"
