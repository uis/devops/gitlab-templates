# Gitlab Templates


This repo will hold group-level GitLab templates.

See https://docs.gitlab.com/ee/user/project/description_templates.html#set-group-level-description-templates for more information.
